<?php
	class Site {
		
		////////////////////////////////////////////////////////////////
		// DEV / DEBUG /////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		public static function sp( $q, $hide = false, $title = false ) {
			
			echo '<pre '.(($hide)?'style="display:none;"':'').'>';
			if ( $title ) echo $title.'<br />-------------------------------<br />';
			print_r( $q );
			echo '</pre>';
        }
        
        public static function sd( $q, $hide = false, $title = false ) {
			
			echo '<pre '.(($hide)?'style="display:none;"':'').'>';
			if ( $title ) echo $title.'<br />-------------------------------<br />';
			var_dump( $q );
			echo '</pre>';
        }
		
		public static function VPost( $POST ) {
			
			foreach ( $POST as $k => $v ) $POST[stripslashes( htmlspecialchars( strip_tags( trim( $k ))))] = stripslashes( htmlspecialchars( strip_tags( trim( $v ))));
			return ( !empty($POST) ) ? $POST : false;
		}
		
		
		////////////////////////////////////////////////////////////////
		// Phone  //////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		public static function phoneIn( $phone ) {
			
			// +7 (111) 111-11-11 -> 71111111111
			
			$phone = preg_replace('/[^0-9]/', '', $phone);
			$phone = mb_substr($phone, 0, 11);
			
			if ( mb_strlen($phone) == 10 ) $phone = '7'.$phone;
			if ( mb_strlen($phone) == 7 ) $phone = '7861'.$phone;
			if ( $phone[0] == '8' ) $phone = '7'.mb_substr($phone, 1);
			
			return $phone;
		}
		
		public static function phoneOut( $str ) {
			
			// 71111111111 -> +7 (111) 111-11-11
			
			$str = self::phoneIn( $str );
			
			for ($k = 0; $k < mb_strlen((string)$str); $k++) $phone[] = mb_substr($str, $k, 1);
			return '+'.$phone[0].' ('.$phone[1].$phone[2].$phone[3].') '.$phone[4].$phone[5].$phone[6].'-'.$phone[7].$phone[8].'-'.$phone[9].$phone[10];
		}
		
		
		////////////////////////////////////////////////////////////////
		// Words  //////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		public static function getWorld( $q = 0, $flag = 'd' ) {
			
			$res = [
				'd' => ['день', 'дня', 'дней'],
				'h' => ['час', 'часа', 'часов'],
				'm' => ['минута', 'минуты', 'минут'],
				's' => ['секунда', 'секунды', 'секунд'],
				'a' => ['автомобиль', 'автомобиля', 'автомобилей'],
			];
			
			$test = [
				[1, 21, 31, 41, 51, 61, 71, 81, 91, 101, 121, 131, 141, 151, 161, 171, 181, 191],
				[2,3,4,22,23,24,32,33,34,42,43,44,52,53,54,62,63,64,72,73,74,82,83,84,92,93,94,102,103,104,122,123,124,132,133,134,142,143,144,152,153,154,162,163,164,172,173,174,182,183,184,192,193,194]
			];
			
			if ( in_array( (int)$q, $test[0] ) ) return $res[$flag][0];
			if ( in_array( (int)$q, $test[1] ) ) return $res[$flag][1];
			return $res[$flag][2];
		}
		
		
		////////////////////////////////////////////////////////////////
		// Init ////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		
		public function __construct( $arConf = [] ) {
			
			$this->Conf = $arConf;
		}
		
		public function Conf() {
			
			return $this->Conf;
		}
		
		public function Route() {
			
			$url = parse_url($_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'])['path'];
			
			$r = explode('/', $url);
			if ( $r[2] ) return ( array_key_exists($r[2], $this->Conf()['Cars']['Items']) ) ? 'content' : '404';
			return 'content';
		}
		
		public function Cars() {
			
			$url = parse_url($_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'])['path'];
			
			$r = explode('/', $url);
			return ( $r[2] ) ? [$r[2] => $this->Conf()['Cars']['Items'][$r[2]]] : $this->Conf()['Cars']['Items'];
		}
		
		public function Banner() {
			
			$url = parse_url($_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'])['path'];
			
			$r = explode('/', $url);
			if ( $r[2] ) return ( array_key_exists($r[2], $this->Conf()['Cars']['Items']) ) ? $r[2] : '';
			return '';
		}
		
	}
	
?>