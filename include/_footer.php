
      <div class="container-fluid map">
        <div class="row py-0">
          <div class="col-md-12">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ad8ee68dbd81eff7cdf0b8483d88c776141b3f0b1ff39dce925de3324ffc2bf7b&amp;width=100%&amp;height=540&amp;lang=ru_RU&amp;scroll=false"></script>
          </div>
          <div class="col-md-2">
            <p class="title">Юг-Авто Центр Краснодар</p>
            <p style="margin-bottom: 40px;">Официальный дилер ŠKODA</p>
            <div class="contact">
              <svg xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Map"></use>
              </svg>
              пос. Яблоновский,  ул. Краснодарская, 1
            </div>
            <div class="contact">
              <svg xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Phone"></use>
              </svg>
              <span class="ford_call_phone_1"><a href="tel:+78612127080">+7 (861) 212 70 80</a></span>
            </div>
            <hr />
            <div class="contact">
              <svg xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Map"></use>
              </svg>
              г. Краснодар, ул. Дзержинского, 102
            </div>
            <div class="contact">
              <svg xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Phone"></use>
              </svg>
              <span class="ford_call_phone_1"><a href="tel:+78612032898">+7 (861) 203 28 98</a></span>
            </div>
          </div>
        </div>
      </div>
      
      <div class="container">
        <div class="row footer">
          <div class="col-md-12">
          
            <?php foreach ( $app->Cars() as $car ) echo $car['disclamer']; ?>
            
            <div class="colapse">
			 <p> *Основные условия кредитования ООО «Фольксваген Банк РУС» (далее Банк) на покупку нового ŠKODA RAPID по продукту «Кредит с остаточным платежом на приобретение нового автомобиля» валюта кредита – рубли РФ; сумма кредита от 120 тыс. до 4 млн. руб. Процентная ставка (в % годовых) при сроке от 24 до 36 мес.- от 10,9%, при первоначальном взносе (далее ПВ) от 15% (включительно) и оформлении договора личного страхования, заключаемого в отношении заемщика. При отказе заемщика от заключения договора личного страхования процентная ставка составит – от 14,9%. <br>*Основные условия кредитования ООО «Фольксваген Банк РУС» (далее Банк) на покупку нового ŠKODA OCTAVIA по продукту «Кредит с остаточным платежом на приобретение нового автомобиля» валюта кредита – рубли РФ; сумма кредита от 120 тыс. до 4 млн. руб. Процентная ставка (в % годовых) при сроке от 24 до 36 мес.- от 10,9%, при первоначальном взносе (далее ПВ) от 15% (включительно) и оформлении договора личного страхования, заключаемого в отношении заемщика. При отказе заемщика от заключения договора личного страхования процентная ставка составит – от 14,9%. <br>*Основные условия кредитования ООО «Фольксваген Банк РУС» (далее Банк) на покупку нового ŠKODA KODIAQ по продукту «Кредит с остаточным платежом на приобретение нового автомобиля» валюта кредита – рубли РФ; сумма кредита от 120 тыс. до 4 млн. руб. Процентная ставка (в % годовых) при сроке от 24 до 36 мес.- от 12,9%, при первоначальном взносе (далее ПВ) от 15% (включительно) и оформлении договора личного страхования, заключаемого в отношении заемщика. При отказе заемщика от заключения договора личного страхования процентная ставка составит – от 16,9%. <br>*Основные условия кредитования ООО «Фольксваген Банк РУС» (далее Банк) на покупку нового ŠKODA SUPERB по продукту «Кредит с остаточным платежом на приобретение нового автомобиля» валюта кредита – рубли РФ; сумма кредита от 120 тыс. до 4 млн. руб. Процентная ставка (в % годовых)  при сроке от 24 до 36 мес.- от 12,9%, при первоначальном взносе (далее ПВ) от 15% (включительно) и оформлении договора личного страхования, заключаемого в отношении заемщика. При отказе заемщика от заключения договора личного страхования процентная ставка составит – от 16,9%. <br>
			 Условия кредита действительны на 01.01.2020 и могут быть изменены Банком. Информация по телефону Банка: 8-800-700-75-57 (звонок по России бесплатный). Лицензия ЦБ РФ № 3500, 117485, г. Москва, ул. Обручева, д.30/1, стр.2. www.vwbank.ru. 
			 </p> 
            </div>
            
            <div class="text-center"><p class="lead"><a href="#" role="footcol"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ArrowDown"></use></svg></a></p></div>
            
          </div>
          
          <div class="col-md-6">© ŠKODA | Официальный дилер <a href="https://yug-avto.ru" target="_blank">компания «Юг-Авто Центр Краснодар»</a></div>
          <div class="col-md-6"><i class="fa fa-phone" aria-hidden="true"></i> <span class="ford_call_phone_1"><a href="tel:+78612223333">+7 (861) 203 19 86</a></span></div>
        </div>
      </div>
      
    </main>
    
    <?php include __DIR__.'/_svg.php'; ?>
    <?php include __DIR__.'/_modals.php'; ?>
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
    
    <script src="/specials/assets/js/app.js?<?=md5_file(__DIR__.'/assets/js/app.js');?>"></script>
    
    <?php include __DIR__.'/_bottom.body.php'; ?>
  </body>
</html>