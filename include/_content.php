<div class="container-fluid">
  <div class="row" style="border-bottom: 2px solid #c8c8c8;">
    <div class="container">
      <div class="row advantages text-center">
        <div class="col-md-12">
          <h2>Преимущества Škoda в Юг-Авто Центр</h2>
        </div>
        <div class="col-md-2" data-form="">
          <a href="#formCredit" role="modal" <?=((count($app->Cars())==1)?'data="Škoda '.reset($app->Cars())['title'].'"':'')?>>
            <svg xmlns="http://www.w3.org/2000/svg">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Credit"></use>
            </svg>
            <p>Оформление льготного кредита</p>
          </a>
        </div>
        <div class="col-md-2" data-form="">
          <a href="#formTradeIn" role="modal" <?=((count($app->Cars())==1)?'data="Škoda '.reset($app->Cars())['title'].'"':'')?>>
            <svg xmlns="http://www.w3.org/2000/svg">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#TradeIn"></use>
            </svg>
            <p>Спецусловия<br />по Trade-in</p>
          </a>
        </div>
        <div class="col-md-2" data-form="">
          <a href="#formTestDrive" role="modal" <?=((count($app->Cars())==1)?'data="Škoda '.reset($app->Cars())['title'].'"':'')?>>
            <svg xmlns="http://www.w3.org/2000/svg">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#TestDrive"></use>
            </svg>
            <p>Запись на<br />тест-драйв</p>
          </a>
        </div>
        <div class="col-md-2" data-form="">
          <a href="#formPreOrder" role="modal" <?=((count($app->Cars())==1)?'data="Škoda '.reset($app->Cars())['title'].'"':'')?>>
            <svg xmlns="http://www.w3.org/2000/svg">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#PreOrder"></use>
            </svg>
            <p>Оформление предзаказа автомобиля</p>
          </a>
        </div>
        <div class="col-md-2" data-form="">
          <a href="#formInsurance" role="modal" <?=((count($app->Cars())==1)?'data="Škoda '.reset($app->Cars())['title'].'"':'')?>>
            <svg xmlns="http://www.w3.org/2000/svg">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Insurance"></use>
            </svg>
            <p>Оформление страховки</p>
          </a>
        </div>
        <div class="col-md-2" data-form="">
          <a href="#formCorporate" role="modal" <?=((count($app->Cars())==1)?'data="Škoda '.reset($app->Cars())['title'].'"':'')?>>
            <svg xmlns="http://www.w3.org/2000/svg">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Corporate"></use>
            </svg>
            <p>Выгодная корпоративная программа</p>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row specials">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h1>Спецпредложения на ŠKODA!</h1>
          <?php if ( count($app->Cars()) == 1 ) { ?>
          <h2>О цене договоримся!</h2>
          <?php } else { ?>
          <h2>Выберите модель, о цене договоримся!</h2>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php if ( count($app->Cars()) == 1 ) { ?>
<style>
.car:nth-child(even) {
    background: none;
}
</style>
<?php } ?>

<?php foreach ( $app->Cars() as $car ) include __DIR__.'/cars/'.$car['key'].'.php'; ?>

<div class="w-100"></div>

<div class="fork">
  <form class="form-inline" data-event="special">
    <div class="container">
      <div class="row">
        <div class="col-md-12 title">Есть предложение от конкурентов?<br /><span>Просто укажите его</span></div>
        
        <input type="hidden" name="Form" value="formFork" />
        <div class="col-md-1"></div>
        <div class="col-md-3">
          <div class="form-group">
            <select class="form-control" name="Car" style="width:100%">
              <option disabled>Выберите модель *</option>
              <?php foreach ( $app->Cars() as $car ) { ?>
              <option value="<?=$car['title']?>" <?=((count($app->Cars())==1)?'selected':'')?>>ŠKODA <?=$car['title']?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <input type="text" class="form-control" style="width:100%" name="Offer" placeholder="Напишите предложение конкурентов *">
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <input type="phone" class="form-control" style="width:100%" name="Phone" placeholder="Ваш номер телефона *">
          </div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4"></div>
        <div class="col-md-4 text-center">
          <a role="submit" href="#">Получить лучшее предложение</a>
        </div>
        <div class="col-md-4"></div>
        
      </div>
    </div>
  </form>
  
  <div class="container">
    <div class="row">
      <div class="alert alert-dismissible alert-success">
      Спасибо за Вашу заявку! Мы свяжемся с Вами в ближайшее время.
      </div>
      
      <div class="alert alert-dismissible alert-danger">
      Ой, что-то пошло не так. Повторите попытку позднее.
      </div>
    </div>
  </div>
  
</div>