<!doctype html>
<html lang="ru">
  
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Суперпредложение на Skoda</title>

    <!-- Bootstrap core CSS -->
    <?php /* <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" /> */ ?>
    <link href="/specials/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal-default-theme.min.css" />
    
    <link href="/specials/assets/fonts/fonts.css?<?=md5_file(__DIR__.'/assets/fonts/fonts.css');?>" rel="stylesheet">
    <link href="/specials/assets/css/app.css?<?=md5_file(__DIR__.'/assets/css/app.css');?>" rel="stylesheet">
    
    <link rel="apple-touch-icon" sizes="57x57" href="/specials/assets/images/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/specials/assets/images/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/specials/assets/images/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/specials/assets/images/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/specials/assets/images/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/specials/assets/images/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/specials/assets/images/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/specials/assets/images/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/specials/assets/images/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/specials/assets/images/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/specials/assets/images/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/specials/assets/images/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/specials/assets/images/icon/favicon-16x16.png">
    <link rel="manifest" href="/specials/assets/images/icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/specials/assets/images/icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <?php include __DIR__.'/_bottom.head.php'; ?>
    
  </head>

  <body>
  	
    <style>
		.widget_callback {width: 300px!important;}
    </style>
  	
    <?php include __DIR__.'/_top.body.php'; ?>
    
    <main role="main">
      
      <div class="logo-row">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <a href="/specials/<?=((count($app->Cars())==1)?reset($app->Cars())['key'].'/':'')?>" class="slogo">
                <img src="/specials/assets/images/LogoS.png" />
              </a>
            </div>
          </div>
        </div>
      </div>
      
      <div class="menu-row">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <a href="#formBestPrice">Акция</a> | <a href="#formCredit">Рассчитать кредит</a> | <a href="#formTradeIn">Обменять авто с пробегом</a> | <a href="#formTestDrive">Запись на тест-драв</a> | <a href="#">Контакты</a>
            </div>
          </div>
        </div>
      </div>
      
      <style>
      	.banner {background: url(<?=$app->Conf()['Banners']['web']?>/<?=$app->Banner()?>_1920x640.jpg?<?=$app->Conf()['Banners']['v']?>) top center no-repeat;}
		@media (max-width:1024px) {
			.banner {background: url(<?=$app->Conf()['Banners']['web']?>/<?=$app->Banner()?>_1024x768.jpg?<?=$app->Conf()['Banners']['v']?>) top center no-repeat;}
		}
		@media (max-width:768px) {
			.banner {background: url(<?=$app->Conf()['Banners']['web']?>/<?=$app->Banner()?>_768x1024.jpg?<?=$app->Conf()['Banners']['v']?>) top center no-repeat;}
		}
		@media (max-width:736px) {
			.banner {
				background: url(<?=$app->Conf()['Banners']['web']?>/<?=$app->Banner()?>_375x281.jpg?<?=$app->Conf()['Banners']['v']?>) top center no-repeat;
				background-size: cover;
			}
		}
		@media (max-width: 736px) and (max-height: 414px) {
			.banner {
				background: url(<?=$app->Conf()['Banners']['web']?>/<?=$app->Banner()?>_375x281.jpg?<?=$app->Conf()['Banners']['v']?>) top center no-repeat;
				background-size: contain;
				background-position: bottom;
			}
		}
		@media (max-width: 414px) {
			.banner {
				background: url(<?=$app->Conf()['Banners']['web']?>/<?=$app->Banner()?>_375x281.jpg?<?=$app->Conf()['Banners']['v']?>) top center no-repeat;
				background-size: contain;
				background-position: bottom;
			}
		}
		@media (max-width: 375px) and (max-height: 667px) {
			.banner {
				background: url(<?=$app->Conf()['Banners']['web']?>/<?=$app->Banner()?>_375x281.jpg?<?=$app->Conf()['Banners']['v']?>) top center no-repeat;
				height: 414px;
				background-size: contain;
				background-position: bottom;
			}
		}
		@media (max-width: 320px) {
			.banner {
				background: url(<?=$app->Conf()['Banners']['web']?>/<?=$app->Banner()?>_320x240.jpg?<?=$app->Conf()['Banners']['v']?>) top center no-repeat;
				background-position: bottom;
			}
		}
      </style>
      
      <div class="row banner">
        <div class="container">
          
          <div class="row top">
            <div class="col-md-5">
              <p class="title">Юг-Авто Центр Краснодар</p>
              <p>Официальный дилер ŠKODA</p>
            </div>
            <div class="col-md-4 address">
              <svg xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Map"></use>
              </svg>
              <p>г. Краснодар, ул. Дзержинского, 102</p>
              <p>п. Яблоновский, ул. Краснодарская, 3</p>
            </div>
            <div class="col-md-3 phones text-right">
              <svg xmlns="http://www.w3.org/2000/svg" style="left: unset;  right: 150px;">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Phone"></use>
              </svg>
              <p><a href="tel:+78612032898">+7 (861) 203 28 98</a></p>
              <p><a href="tel:+78612127080">+7 (861) 212 70 80</a></p>
            </div>
          </div>
          
          <div class="row banner-button">
            <div class="col-md-4">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ArrowLeft"></use>
              </svg>
            </div>
            <div class="col-md-4 text-center">
              <a href="#formBestPrice">Получить максимальную выгоду</a>
            </div>
            <div class="col-md-4">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ArrowRight"></use>
              </svg>
            </div>
          </div>
          
        </div>
      </div>