<div class="car">
  <div class="container">
    <div class="row">
      <div class="col-md-7 info text-center">
        
        <img src="/specials/assets/images/kodiaq.png" />
        
        <div class="icons">
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Dashboard"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>8 - 10.5 с</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Fuel"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Бензин / Дизель</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Kpp"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Автомат /</li>
                <li>Механика</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Engine"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Мощность: 125 л.с. /</li>
                <li>150 л.с. / 180 л.с.</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Petrol"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>от 6.8 л. город /</li>
                <li>5.2 л. трасса</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Seat"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>5 / 7 мест</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Safeness"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Доказал свою</li>
                <li>безопасность</li>
              </ul>
            </span>
          </div>
          
          
        </div>
        
      </div>
      <div class="col-md-4 buttons">
      
      	<h2>ŠKODA Kodiaq</h2>
        <h3>Выгода до 235 800 ₽<sup>*</sup></h3>
        
        <a href="#formCredit" role="modal" data="Škoda Kodiaq">За 13 000 руб./мес. в кредит <sup>1</sup></a>
        <a href="#formTestDrive" role="modal" data="Škoda Kodiaq">Автомобили с ПТС в наличии</a>
        <a href="#formTradeIn" role="modal" data="Škoda Kodiaq">Обмен авто с пробегом</a>
        <a href="#formBestPrice" role="modal" data="Škoda Kodiaq">Получить максимальную выгоду</a>
      </div>
      <div class="col-md-1"></div>
    </div>
  </div>
</div>