<div class="car">
  <div class="container">
    <div class="row">
      <div class="col-md-7 info text-center">
        
        <img src="/specials/assets/images/octavia.png" />
        
        <div class="icons">
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Dashboard"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>8.2 с</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Fuel"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Бензин</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Engine"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Мощность: 110 л.с. /</li>
                <li>150 л.с. / 190 л.с. / 230 л.с.</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Kpp"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Автомат /</li>
                <li>Механика</li>
              </ul>
            </span>
          </div>
          
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Chassis"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Привод</li>
                <li>Передний / Полный</li>
              </ul>
            </span>
          </div>
          
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Safeness"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Доказал свою</li>
                <li>безопасность</li>
              </ul>
            </span>
          </div>
          
          
        </div>
        
      </div>
      <div class="col-md-4 buttons">
      
      	<h2>ŠKODA Octavia</h2>
        <h3>Выгода до 192 400 ₽<sup>*</sup></h3>
        
        <a href="#formCredit" role="modal" data="Škoda Octavia">За 9 000 руб./мес. в кредит <sup>1</sup></a>
        <a href="#formTestDrive" role="modal" data="Škoda Octavia">Автомобили с ПТС в наличии</a>
        <a href="#formTradeIn" role="modal" data="Škoda Octavia">Обмен авто с пробегом</a>
        <a href="#formBestPrice" role="modal" data="Škoda Octavia">Получить максимальную выгоду</a>
      </div>
      <div class="col-md-1"></div>
    </div>
  </div>
</div>