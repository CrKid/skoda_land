<div class="car">
  <div class="container">
    <div class="row">
      <div class="col-md-7 info text-center">
        
        <img src="/specials/assets/images/rapid.png" />
        
        <div class="icons">
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Dashboard"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>9 - 11.6 с</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Fuel"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Бензин</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Kpp"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Автомат /</li>
                <li>Механика</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Engine"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Мощность: 90 л.с. /</li>
                <li>110 л.с. / 125 л. с.</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Petrol"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>от 7 л. город /</li>
                <li>4.3 л. трасса</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Capacity"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Вместимость</li>
                <li>530/1470 л</li>
              </ul>
            </span>
          </div>
          <div class="tth">
            <span class="icon">
              <svg class="ArrowBanner" xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Safeness"></use>
              </svg>
            </span>
            <span class="text">
              <ul>
                <li>Доказал свою</li>
                <li>безопасность</li>
              </ul>
            </span>
          </div>
          
          
        </div>
        
      </div>
      <div class="col-md-4 buttons">
      
      	<h2>ŠKODA Rapid</h2>
        <h3>Выгода до 133 100 ₽<sup>*</sup></h3>
        
        <a href="#formCredit" role="modal" data="Škoda Rapid">За 6 000 руб./мес. в кредит <sup>1</sup></a>
        <a href="#formTestDrive" role="modal" data="Škoda Rapid">Автомобили с ПТС в наличии</a>
        <a href="#formTradeIn" role="modal" data="Škoda Rapid">Обмен авто с пробегом</a>
        <a href="#formBestPrice" role="modal" data="Škoda Rapid">Получить максимальную выгоду</a>
      </div>
      <div class="col-md-1"></div>
    </div>
    <div class="w-100"></div>
  </div>
</div>