<div class="remodal text-left" data-remodal-id="formBestPrice">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h3>Получить максимальную выгоду</h3>
    <legend role="car"><?php if ( count($app->Cars()) == 1 ) { ?>НА ŠKODA <?=reset($app->Cars())['title']?><?php } ?></legend>
    <p>* – поля, обязательные для заполнения</p>
    <form data-event="special">
    	<input type="hidden" name="Form" value="formBestPrice" />
        <fieldset>
            <div class="form-group">
                <input type="text" class="form-control" name="Name" placeholder="Имя *">
            </div>
            <div class="form-group">
                <input type="phone" class="form-control" name="Phone" placeholder="Телефон *">
            </div>
            <div class="form-group">
                <select class="form-control" name="Dc">
                  <option>Выберите дилерский центр *</option>
				  <option value="Юг-Авто Центр п. Яблоновский">Юг-Авто Центр п. Яблоновский</option>
                  <option value="Юг-Авто Центр Краснодар">Юг-Авто Центр Краснодар</option>
                  
                </select>
            </div>
        </fieldset>
        <div class="form-group">
        	<p>Отправляя заявку, вы соглашаетесь на обработку <a href="/about/agreement/" target="_blank">персональных данных</a>.</p>
        </div>
        <fieldset>
        	<button type="submit" role="sendForm" class="btn btn-success">Отправить</button>
        </fieldset>
    </form>
    <div class="alert alert-dismissible alert-success">
    Спасибо за Вашу заявку! Мы свяжемся с Вами в ближайшее время.
    </div>
    
    <div class="alert alert-dismissible alert-danger">
    Ой, что-то пошло не так. Повторите попытку позднее.
    </div>
</div>


<div class="remodal text-left" data-remodal-id="formCredit">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h3>Оформление льготного кредита</h3>
    <legend role="car"><?php if ( count($app->Cars()) == 1 ) { ?>НА ŠKODA <?=reset($app->Cars())['title']?><?php } ?></legend>
    <p>* – поля, обязательные для заполнения</p>
    <form data-event="credit">
    	<input type="hidden" name="Form" value="formCredit" />
        <fieldset>
            <div class="form-group">
                <input type="text" class="form-control" name="Name" placeholder="Имя *">
            </div>
            <div class="form-group">
                <input type="phone" class="form-control" name="Phone" placeholder="Телефон *">
            </div>
            <div class="form-group">
                <select class="form-control" name="Dc">
                  <option>Выберите дилерский центр *</option>
				  <option value="Юг-Авто Центр п. Яблоновский">Юг-Авто Центр п. Яблоновский</option>
                  <option value="Юг-Авто Центр Краснодар">Юг-Авто Центр Краснодар</option>
                  
                </select>
            </div>
        </fieldset>
        <div class="form-group">
        	<p>Отправляя заявку, вы соглашаетесь на обработку <a href="/about/agreement/" target="_blank">персональных данных</a>.</p>
        </div>
        <fieldset>
        	<button type="submit" role="sendForm" class="btn btn-success">Отправить</button>
        </fieldset>
    </form>
    <div class="alert alert-dismissible alert-success">
    Спасибо за Вашу заявку! Мы свяжемся с Вами в ближайшее время.
    </div>
    
    <div class="alert alert-dismissible alert-danger">
    Ой, что-то пошло не так. Повторите попытку позднее.
    </div>
</div>

<div class="remodal text-left" data-remodal-id="formTradeIn">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h3>Спецусловия по Trade-in</h3>
    <legend role="car"><?php if ( count($app->Cars()) == 1 ) { ?>НА ŠKODA <?=reset($app->Cars())['title']?><?php } ?></legend>
    <p>* – поля, обязательные для заполнения</p>
    <form data-event="sell-car">
        <input type="hidden" name="Form" value="formTradein" />
        <fieldset>
            <div class="form-group">
                <input type="text" class="form-control" name="Name" placeholder="Имя *">
            </div>
            <div class="form-group">
                <input type="phone" class="form-control" name="Phone" placeholder="Телефон *">
            </div>
            <div class="form-group">
                <select class="form-control" name="Dc">
                  <option>Выберите дилерский центр *</option>
				  <option value="Юг-Авто Центр п. Яблоновский">Юг-Авто Центр п. Яблоновский</option>
                  <option value="Юг-Авто Центр Краснодар">Юг-Авто Центр Краснодар</option>
                  
                </select>
            </div>
        </fieldset>
        <div class="form-group">
        	<p>Отправляя заявку, вы соглашаетесь на обработку <a href="/about/agreement/" target="_blank">персональных данных</a>.</p>
        </div>
        <fieldset>
        	<button type="submit" role="sendForm" class="btn btn-success">Отправить</button>
        </fieldset>
    </form>
    <div class="alert alert-dismissible alert-success">
    Спасибо за Вашу заявку! Мы свяжемся с Вами в ближайшее время.
    </div>
    
    <div class="alert alert-dismissible alert-danger">
    Ой, что-то пошло не так. Повторите попытку позднее.
    </div>
</div>

<div class="remodal text-left" data-remodal-id="formTestDrive">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h3>Записаться на тест-драйв</h3>
    <legend role="car"><?php if ( count($app->Cars()) == 1 ) { ?>НА ŠKODA <?=reset($app->Cars())['title']?><?php } ?></legend>
    <p>* – поля, обязательные для заполнения</p>
    <form data-event="test-drive">
        <input type="hidden" name="Form" value="formTestdrive" />
        <fieldset>
            <div class="form-group">
                <input type="text" class="form-control" name="Name" placeholder="Имя *">
            </div>
            <div class="form-group">
                <input type="phone" class="form-control" name="Phone" placeholder="Телефон *">
            </div>
            <div class="form-group">
                <select class="form-control" name="Dc">
                  <option>Выберите дилерский центр *</option>
				  <option value="Юг-Авто Центр п. Яблоновский">Юг-Авто Центр п. Яблоновский</option>
                  <option value="Юг-Авто Центр Краснодар">Юг-Авто Центр Краснодар</option>
                  
                </select>
            </div>
        </fieldset>
        <div class="form-group">
        	<p>Отправляя заявку, вы соглашаетесь на обработку <a href="/about/agreement/" target="_blank">персональных данных</a>.</p>
        </div>
        <fieldset>
        	<button type="submit" role="sendForm" class="btn btn-success">Отправить</button>
        </fieldset>
    </form>
    <div class="alert alert-dismissible alert-success">
    Спасибо за Вашу заявку! Мы свяжемся с Вами в ближайшее время.
    </div>
    
    <div class="alert alert-dismissible alert-danger">
    Ой, что-то пошло не так. Повторите попытку позднее.
    </div>
</div>

<div class="remodal text-left" data-remodal-id="formPreOrder">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h3>Оформление предзаказа автомобиля</h3>
    <legend role="car"><?php if ( count($app->Cars()) == 1 ) { ?>НА ŠKODA <?=reset($app->Cars())['title']?><?php } ?></legend>
    <p>* – поля, обязательные для заполнения</p>
    <form data-event="car">
        <input type="hidden" name="Form" value="formPreOrder" />
        <fieldset>
            <div class="form-group">
                <input type="text" class="form-control" name="Name" placeholder="Имя *">
            </div>
            <div class="form-group">
                <input type="phone" class="form-control" name="Phone" placeholder="Телефон *">
            </div>
            <div class="form-group">
                <select class="form-control" name="Dc">
                  <option>Выберите дилерский центр *</option>
				  <option value="Юг-Авто Центр п. Яблоновский">Юг-Авто Центр п. Яблоновский</option>
                  <option value="Юг-Авто Центр Краснодар">Юг-Авто Центр Краснодар</option>
                  
                </select>
            </div>
        </fieldset>
        <div class="form-group">
        	<p>Отправляя заявку, вы соглашаетесь на обработку <a href="/about/agreement/" target="_blank">персональных данных</a>.</p>
        </div>
        <fieldset>
        	<button type="submit" role="sendForm" class="btn btn-success">Отправить</button>
        </fieldset>
    </form>
    <div class="alert alert-dismissible alert-success">
    Спасибо за Вашу заявку! Мы свяжемся с Вами в ближайшее время.
    </div>
    
    <div class="alert alert-dismissible alert-danger">
    Ой, что-то пошло не так. Повторите попытку позднее.
    </div>
</div>

<div class="remodal text-left" data-remodal-id="formInsurance">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h3>Оформление страховки</h3>
    <legend role="car"><?php if ( count($app->Cars()) == 1 ) { ?>НА ŠKODA <?=reset($app->Cars())['title']?><?php } ?></legend>
    <p>* – поля, обязательные для заполнения</p>
    <form data-event="insurance">
        <input type="hidden" name="Form" value="formInsurance" />
        <fieldset>
            <div class="form-group">
                <input type="text" class="form-control" name="Name" placeholder="Имя *">
            </div>
            <div class="form-group">
                <input type="phone" class="form-control" name="Phone" placeholder="Телефон *">
            </div>
            <div class="form-group">
                <select class="form-control" name="Dc">
                  <option>Выберите дилерский центр *</option>
				  <option value="Юг-Авто Центр п. Яблоновский">Юг-Авто Центр п. Яблоновский</option>
                  <option value="Юг-Авто Центр Краснодар">Юг-Авто Центр Краснодар</option>
                  
                </select>
            </div>
        </fieldset>
        <div class="form-group">
        	<p>Отправляя заявку, вы соглашаетесь на обработку <a href="/about/agreement/" target="_blank">персональных данных</a>.</p>
        </div>
        <fieldset>
        	<button type="submit" role="sendForm" class="btn btn-success">Отправить</button>
        </fieldset>
    </form>
    <div class="alert alert-dismissible alert-success">
    Спасибо за Вашу заявку! Мы свяжемся с Вами в ближайшее время.
    </div>
    
    <div class="alert alert-dismissible alert-danger">
    Ой, что-то пошло не так. Повторите попытку позднее.
    </div>
</div>

<div class="remodal text-left" data-remodal-id="formCorporate">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h3>Выгодная корпоративная программа</h3>
    <legend role="car"><?php if ( count($app->Cars()) == 1 ) { ?>НА ŠKODA <?=reset($app->Cars())['title']?><?php } ?></legend>
    <p>* – поля, обязательные для заполнения</p>
    <form data-event="car">
        <input type="hidden" name="Form" value="formCorporate" />
        <fieldset>
            <div class="form-group">
                <input type="text" class="form-control" name="Name" placeholder="Имя *">
            </div>
            <div class="form-group">
                <input type="phone" class="form-control" name="Phone" placeholder="Телефон *">
            </div>
            <div class="form-group">
                <select class="form-control" name="Dc">
                  <option>Выберите дилерский центр *</option>
				  <option value="Юг-Авто Центр п. Яблоновский">Юг-Авто Центр п. Яблоновский</option>
                  <option value="Юг-Авто Центр Краснодар">Юг-Авто Центр Краснодар</option>
                  
                </select>
            </div>
        </fieldset>
        <div class="form-group">
        	<p>Отправляя заявку, вы соглашаетесь на обработку <a href="/about/agreement/" target="_blank">персональных данных</a>.</p>
        </div>
        <fieldset>
        	<button type="submit" role="sendForm" class="btn btn-success">Отправить</button>
        </fieldset>
    </form>
    <div class="alert alert-dismissible alert-success">
    Спасибо за Вашу заявку! Мы свяжемся с Вами в ближайшее время.
    </div>
    
    <div class="alert alert-dismissible alert-danger">
    Ой, что-то пошло не так. Повторите попытку позднее.
    </div>
</div>