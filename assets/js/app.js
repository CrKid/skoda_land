'use strict';

$('input[type="phone"]').inputmask({'mask': '+7 (999) 999-99-99', showMaskOnHover: false});

$('a[role="modal"]').click( function() {
	
	if ( typeof $(this).attr('data') != 'undefined' ) {
		
		$('legend[role="car"]').text('на '+$(this).attr('data'));
		
	} else {
		
		$('legend[role="car"]').text('');
	}
});

$('a[role="personal"]').click( function() {
	
	$(this).parent().parent().parent().parent().parent().children('p[role="personal"]').slideToggle(300);
	return false;
});

$('a[role="footcol"]').click( function() {
	
	$('div.colapse').slideToggle(300);
	$(this).toggleClass('active');
	return false;
});

$('[role="sendForm"]').click( function() {
	
	var send = {};
	var but = $(this);
	var form = $(this).parent().parent();
	var success = $(but).parent().parent().parent().find('.alert-success');
	var error = $(but).parent().parent().parent().find('.alert-danger');
	var Event = $(form).data('event');
	
	send.Form = $(form).parent().children('h3').text()+' '+$(form).parent().children('legend[role="car"]').text();
	send.Name = $(form).find('input[name="Name"]').val();
	send.Phone = $(form).find('input[name="Phone"]').val();
	send.Dc = $(form).find('select[name="Dc"]').val();
	send.Car = $(form).parent().find('legend[role="car"]').text();
	
	send.AppName = 'Lands';
    send.EventCategory = 'Заполена форма';
	
	if ( send.Phone && send.Name && send.Dc ) {
		
		$.ajax({
			type: "POST",
			url: '/specials/ajax/',  
			data: send,
			success: function(data){
				
				let res = JSON.parse( data );
				
				if ( res.status == 'success' ) {
					
					$(form).slideUp(300);
					$(success).slideDown(300);
					$(error).hide();
					
					setTimeout(function() {
						
						$(form).slideDown(300);
						$(success).slideUp(300);
						
					}, 5000);
					
                    if (typeof Matomo != 'undefined') _paq.push(["trackEvent", 'Формы лэндинга', send.Form, 'Send']);
						
					var Layer = {
						'event': 'FormSubmission',
						'eventCategory': Event,
						'eventAction': 'submit',
						'eventLabel': send.Car || null
					};
					dataLayer.push( Layer );
					
					var CallTouchURL = 'https://api-node7.calltouch.ru/calls-service/RestAPI/requests/20907/register/';
					CallTouchURL += '?subject=Формы лэндинга - '+send.Form;
					CallTouchURL += '&sessionId='+window.call_value_12;
					CallTouchURL += '&fio='+send.Name;
					CallTouchURL += '&phoneNumber='+send.Phone.replace(/[^\d;]/g, '');
					
					$.get( CallTouchURL );
					
					YApps.AppPushStat( send );
					
				} else {
					
					$(error).slideDown(300);
				}
				
				$(form).find('input[name="Name"]').removeClass('is-invalid');
				$(form).find('input[name="Phone"]').removeClass('is-invalid');
				$(form).find('select[name="Dc"]').removeClass('is-invalid');
			},
			error: function() {
				
				$(error).slideDown(300);
				
				setTimeout(function() {
						
					$(error).slideUp(300);
					
				}, 5000);
			}
		});
	
	} else {
		
		if ( !send.Name ) $(form).find('input[name="Name"]').addClass('is-invalid');
		if ( !send.Phone ) $(form).find('input[name="Phone"]').addClass('is-invalid');
		if ( !send.Dc ) $(form).find('select[name="Dc"]').addClass('is-invalid');
	}
	
	return false;
});

$('[role="submit"]').click( function() {
	
	var send = {};
	var but = $(this);
	var form = $(this).parent().parent().parent().parent();
	var success = $(form).parent().find('.alert-success');
	var error = $(form).parent().find('.alert-danger');
	
	send.Form = $(form).find('input[name="Form"]').val();
	send.Offer = $(form).find('input[name="Offer"]').val();
	send.Phone = $(form).find('input[name="Phone"]').val();
	send.Car = $(form).find('select[name="Car"]').val();
	
	send.AppName = 'Lands';
    send.EventCategory = 'Заполена форма';
	
	if ( send.Phone && send.Offer && send.Car ) {
		
		$.ajax({
			type: "POST",
			url: '/specials/ajax/',  
			data: send,
			success: function(data){
				
				let res = JSON.parse( data );
				
				if ( res.status == 'success' ) {
					
					$(form).slideUp(300);
					$(success).slideDown(300);
					$(error).hide();
					
					yaCounter6254605.reachGoal('FordLandind_send');
					ga('send', 'event', 'Формы лэндинга', send.Form, 'Send');
					_paq.push(["trackEvent", 'Формы лэндинга', send.Form, 'Send']);							
					
					var CallTouchURL = 'https://api-node7.calltouch.ru/calls-service/RestAPI/requests/20907/register/';
					CallTouchURL += '?subject='+send.Form;
					CallTouchURL += '&sessionId='+window.call_value_12;
					CallTouchURL += '&fio='+send.Name;
					CallTouchURL += '&phoneNumber='+send.Phone.replace(/[^\d;]/g, '');
					
					$.get( CallTouchURL );
					
					YApps.AppPushStat( send );
					
				} else {
					
					$(error).slideDown(300);
				}
				
				$(form).find('input[name="Offer"]').removeClass('is-invalid');
				$(form).find('select[name="Car"]').removeClass('is-invalid');
				$(form).find('input[name="Phone"]').removeClass('is-invalid');
			},
			error: function() {
				
				$(error).slideUp(300);
			}
		});
	
	} else {
		
		if ( !send.Offer ) $(form).find('input[name="Offer"]').addClass('is-invalid');
		if ( !send.Phone ) $(form).find('input[name="Phone"]').addClass('is-invalid');
		if ( !send.Car ) $(form).find('select[name="Car"]').addClass('is-invalid');
	}
	
	return false;
});