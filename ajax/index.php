<?php
	ini_set('display_errors','Off');
	error_reporting('E_ALL');
	
	//Load Composer's autoloader
	require __DIR__.'/../vendor/autoload.php';
	
	
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	
	if ( $_POST ) {
		
		$mail = new PHPMailer(true);
		
		try {
		
			//Recipients
			$mail->setFrom('landing@skoda.yug-avto.ru', 'Form Sender');
			$mail->addAddress('callcenter@adv.yug-avto.ru', '');
			$mail->addAddress('yuliya.stolbovya@yug-avto.ru', '');
			
			//$mail->addAddress('anton.boreckiy@yug-avto.ru', '');
			
			$mail->CharSet = 'UTF-8';
		
			//Content
			$mail->isHTML(true);
			
			$mail->Body    = 'Заполнена форма "'.$_POST['Form'].'"';
			$mail->Subject = 'Заявка с лэндинга Skoda. Форма: '.$_POST['Form'];
			
			
			$mail->Body .= '<br /><br />';
			
			if ( $_POST['Name'] ) $mail->Body .= '<strong>Имя</strong>: '. $_POST['Name'].'<br />';
			if ( $_POST['Phone'] ) $mail->Body .= '<strong>Телефон</strong>: '. $_POST['Phone'].'<br />';
			if ( $_POST['Offer'] ) $mail->Body .= '<strong>Предложение конкурентов</strong>: '. $_POST['Offer'].'<br />';
			if ( $_POST['Dc'] ) $mail->Body .= '<strong>Дилерский центр</strong>: '. $_POST['Dc'].'<br />';
			if ( $_POST['Car'] ) $mail->Body .= '<strong>Автомобиль</strong>: '. $_POST['Car'].'<br />';
		
			$mail->send();
			echo json_encode(['status'=>'success']);
		
		} catch (Exception $e) {
			
			echo json_encode(['status'=>'error']);
		}
	}

?>