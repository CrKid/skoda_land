<?php
	/*
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	*/
	
	ob_start();
	
	$arConf = require __DIR__.'/vendor/Config.php';
	require_once __DIR__.'/vendor/Site.php';
	
	$app = new Site( $arConf );
	
	include __DIR__.'/include/_header.php';
	include __DIR__.'/include/_'.$app->Route().'.php';
	include __DIR__.'/include/_footer.php';
?>

      
      
      